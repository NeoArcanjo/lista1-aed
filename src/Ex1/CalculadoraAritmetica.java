/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ex1;

/**
 *
 * @author arcanjo
 */
class CalculadoraAritmetica {
    /**
     * Faça um programa no qual o usuário possa escolher uma das opções:
     * “1 - Multiplicação de inteiros”, 
     * “2 - Divisão de inteiros”, 
     * “3 - Adição de inteiros”, 
     * “4 - Subtração de inteiros” e 
     * “5 – Sair do programa”.
     * Caso o usuário digite 5, ele poderá entrar com dois números.
     * O resultado da operação matemática entre eles deverá ser exibido na tela 
     * e, logo em seguida, retornar ao menu principal.
     * Para o caso de divisão por 0, apresentar a devida mensagem de erro para 
     * o usuário.
     * Nome do programa: CalculadoraAritmetica.java
     * 
     * @param fator1
     * @param fator2
     * @return 
     */
    
    public static int multiplicacao(Integer fator1, Integer fator2){
        return fator1 * fator2;
    }
    
    public static int divisao(Integer divisor, Integer dividendo){
        try{
            return divisor/dividendo;
        }
        catch(ArithmeticException Ex){
            System.out.println("Invalida divisao por zero");
            System.exit(1);
        }
        return 0;
    }
    
    public static int adicao(Integer[] nums){
        var soma = 0;
        for(var x : nums){
            soma += x;
        }
        return soma;
    }
    
    public static int adicao(Integer num1, Integer num2){
        return num1 + num2;
    }
    
    public static int subtracao(Integer p1,  Integer p2){
        return p1 - p2;
    }
}
