/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ex1;

import java.util.Scanner;


/**
 *
 * @author arcanjo
 */
public class NumerosNaturaisImpares {
    
    public static void gen(){
        // TODO code application logic here
        /**
         * 1. Dado um número inteiro positivo n, crie um programa em java para 
         * imprimir os n primeiros naturais ímpares. Exemplo: para n=4 a saída 
         * deverá ser 1,3,5,7. Nome do programa: NumerosNaturaisImpares.java
         */
        
        var dados = new Scanner(System.in);
        System.out.println("Informe quantos impares voce quer: ");
        
        int lim = dados.nextInt();
        int base = 0;
        for(int i = 0; i < lim; i++){
            System.out.println(i+1 + ": " + ((2 * base++) + 1)  );
        }
    }
}
