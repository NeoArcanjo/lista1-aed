/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ex1;

import java.io.File;
import java.io.FileNotFoundException;
import static java.lang.Double.parseDouble;
import java.util.Scanner;

/**
 *
 * @author arcanjo
 */
public class Exercicio4 {
    /**
     * Uma loja possui 4 filiais, cada uma com um código de 1 a 4.
Um arquivo contendo todas as vendas feitas durante o dia nas quatro filiais é gerado a
partir de uma planilha, sendo que cada linha do arquivo contém o número da filial e o valor
de uma venda efetuada, separados por vírgula.
Ex.:
1,189.90
1,45.70
3,29.90
4,55.00

No exemplo acima, a filial 1 fez duas vendas, a primeira
totalizando R$ 189,90 e a segunda R$ 45,70. A filial 3 fez
uma venda de R$ 29,90 e a 4 também uma de R$ 55,00.
Faça um programa que leia este arquivo e informe, ao
final, o total e o valor médio das vendas de cada filial.
     */
    
    
    static void vendasPorFilial(String filename){
        try{
            var file = new File(filename);
            var sc = new Scanner(file).useDelimiter(",|\n");
            int filial;
            double venda;
            double totalVendas1 = 0.00, totalVendas2 = 0.00, totalVendas3 = 0.00, totalVendas4 = 0.00; 
            double mediaVendas1, mediaVendas2, mediaVendas3, mediaVendas4; 
            int cont1 = 0, cont2 = 0, cont3 = 0, cont4 = 0;
            
            while(sc.hasNext()){
                filial = sc.nextInt();
                venda = parseDouble(sc.next());
                
                switch(filial){
                    case 1:
                        totalVendas1 += venda;
                        cont1++;
                        break;
                    case 2:
                        totalVendas2 += venda;
                        cont2++;
                        break;
                    case 3:
                        totalVendas3 += venda;
                        cont3++;
                        break;
                    case 4:
                        totalVendas4 += venda;
                        cont4++;
                        break;
                }
            }

            mediaVendas1 = cont1 > 0 ? totalVendas1/cont1 : 0.0;
            mediaVendas2 = cont2 > 0 ? totalVendas2/cont2 : 0.0;
            mediaVendas3 = cont3 > 0 ? totalVendas3/cont3 : 0.0;
            mediaVendas4 = cont4 > 0 ? totalVendas4/cont4 : 0.0;

            System.out.println("A filial 1 vendeu R$" + totalVendas1 + ". Sua media de vendas foi de " + mediaVendas1);
            System.out.println("A filial 2 vendeu R$" + totalVendas2 + ". Sua media de vendas foi de " + mediaVendas2);
            System.out.println("A filial 3 vendeu R$" + totalVendas3 + ". Sua media de vendas foi de " + mediaVendas3);
            System.out.println("A filial 4 vendeu R$" + totalVendas4 + ". Sua media de vendas foi de " + mediaVendas4);
        
        }catch(FileNotFoundException e){
            System.out.println(e);
            System.exit(1);
        }
    }
    
//    static void vendasPorFilialOld(String filename){
//        try{
//            var file = new File(filename);
//            var sc = new Scanner(file);
//            int filial;
//            int filiais = 4;
//            double[] totalVendas = {0.00, 0.00, 0.00, 0.00}; 
//            double[] mediaVendas = {0.00, 0.00, 0.00, 0.00}; 
//            int[] cont = {0, 0, 0, 0};
//            String[] line;
//            
//            while(sc.hasNext()){
//                line = sc.nextLine().split(",");
////                System.out.println(line[1]);
//                filial = parseInt(line[0]) -1;
//                totalVendas[filial] += parseFloat(line[1]);
//                cont[filial]++;
//            }
//            for(int x=0; x<filiais; x++){
//                mediaVendas[x] = cont[x] > 0 ? totalVendas[x]/cont[x] : 0.0;
//            }
//            for(int x=0; x<filiais; x++){
//                System.out.println("A filial " + (x+1) + " vendeu R$" + totalVendas[x]);
//                System.out.println("media da filial " + (x+1) + " foi de " + mediaVendas[x]);
//            }
//        }catch(FileNotFoundException e){
//            System.out.println(e);
//            System.exit(1);
//        }
//    }
}
