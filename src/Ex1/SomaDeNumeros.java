/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ex1;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author arcanjo
 */
public class SomaDeNumeros {
    public static void calc() {
    /**
     * Faça um programa em java que leia vários números. Caso o usuário digite 
     * -1, o programa para de solicitar os números e imprime a soma de todos os
     * números digitados, sem contar com o -1. 
     * Nome do programa: SomaDeNumeros.java
    */
    
    var numeros = new Scanner(System.in);
        
    try{
        
        int num =  0, soma = 0;
        
        System.out.println("Informe números para somar ou -1 para parar");
        
            while(num != -1){

                num = numeros.nextInt();
                if(num != -1){
                    soma += num;
                }
            }
            System.out.println("Soma: " + soma);
            System.exit(0);

        }
        catch(InputMismatchException Ex){
            System.out.println("Reason:" + Ex);
            numeros.close();
            System.exit(1);
        }
    }
}
