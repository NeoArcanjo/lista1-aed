/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ex1;

import static Ex1.CalculadoraAritmetica.adicao;
import static Ex1.CalculadoraAritmetica.divisao;
import static Ex1.CalculadoraAritmetica.multiplicacao;
import static Ex1.CalculadoraAritmetica.subtracao;
import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Scanner;

/**
 *
 * @author arcanjo
 */
public class Gerenciador {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        
//            NumerosNaturaisImpares.gen();
//            SomaDeNumeros.calc();
//try {
//            calculadoraAritmetica();
//} catch (IOException ex) {
//            java.util.logging.Logger.getLogger(Gerenciador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
           Exercicio4.vendasPorFilial("./src/Ex1/vendas.txt");
//            Exercicio5.echo();
//        
    }
    
    public static void calculadoraAritmetica() throws IOException{
        var sc = new Scanner(System.in);
        int num1, num2;
        
        int opt = 0;
        while(opt != 5){
            Runtime.getRuntime().exec("sh -c clear");
            System.out.println("1 - Multiplicação de inteiros");
            System.out.println("2 - Divisão de inteiros");
            System.out.println("3 - Adição de inteiros");
            System.out.println("4 - Subtração de inteiros");
            System.out.println("5 – Sair do programa");
            
            opt = sc.nextInt();
        
            switch(opt){
                case 1:
                    System.out.println("Informe dois numeros para multiplicar");

                    num1 = sc.nextInt();
                    num2 = sc.nextInt();
                    System.out.println("Resultado: " + multiplicacao(num1, num2));
                    break;
                case 2:
                    System.out.println("Informe dois numeros para Dividir");

                    num1 = sc.nextInt();
                    num2 = sc.nextInt();
                    System.out.println("Resultado: " + divisao(num1, num2));
                    break;
                case 3:
                    System.out.println("Informe dois numeros para Somar");

                    num1 = sc.nextInt();
                    num2 = sc.nextInt();
                    System.out.println("Resultado: " + adicao(num1, num2));
                    break;
                case 4:
                    System.out.println("Informe dois numeros para Subtrair");

                    num1 = sc.nextInt();
                    num2 = sc.nextInt();
                    System.out.println("Resultado: " + subtracao(num1, num2));
                    break;
                case 5:
                    System.out.println("Até a proxima");
                    sc.close();
                    System.exit(0);
                    break;
                default:
                    System.out.println("Opcao Invalida!!");
            }
        }
    }
}
