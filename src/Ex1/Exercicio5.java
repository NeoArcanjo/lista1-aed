/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ex1;

import java.util.Scanner;

/**
 *
 * @author arcanjo
 */
public class Exercicio5 {
    /**
     * Escreva um programa em java que receba um nome de arquivo e uma
     * sequência de palavras como argumentos na linha de comando, informe se o
     * arquivo existe ou não, caso não exista, crie-o e, por fim, escreva neste 
     * arquivo a sequência de palavras passadas como argumentos.
     */
    
    static void echo(){
        var sc = new Scanner(System.in);
        System.out.println("echo: \n");
        var filename = sc.next();
        String args = sc.nextLine();
        System.out.println(args);
    }
}
